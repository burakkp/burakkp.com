//Post Mail
app.post('/send', (req, res) => {
  const output = `
    <img src="http://res.cloudinary.com/dphwxyaea/image/upload/v1506070691/logo-arabaparcasi_qkkkw5.png" style="width:100px;" />
    <h3>Aranılan Parça Bilgileri</h3>
    <h4>Araba Bilgileri</h4>
    <ul>
    <li>Marka: ${req.body.brand}</li>
    <li>Model: ${req.body.model}</li>
    <li>Model Ayı: ${req.body.model_month}</li>
    <li>Model Yılı: ${req.body.model_year}</li>
    <li>Araç Tipi: ${req.body.car_Type}</li>
    <li>Beygir Gücü: ${req.body.car_Power}</li>
    <li>Kapı Sayısı: ${req.body.door_count}</li>
    <li>Motor Tipi: ${req.body.machine_type}</li>
    </ul>
    <hr />
    <h4>Araç Sahibinin Bilgileri</h4>
    <ul>
        <li>Adı Soyadı: ${req.body.fullName}</li>
        <li>Telefon: ${req.body.phone}</li>
        <li>Email: ${req.body.email}</li>
        <li>Adres: ${req.body.Address}</li>
    </ul>
    <hr />
    <h4>Aranılan Parça Bilgileri</h4>
    <ul>
        <li>Parça Adı: ${req.body.partName}</li>
        <li>Parça Numarası: ${req.body.partNo}</li>
        <li>Parça Açıklaması: ${req.body.partDetail}</li>
    </ul>
  `;

  let transporter = nodemailer.createTransport({
      host: 'ns25.turkishost.com',
      port: 465,
      secure: true, // true for 465, false for other ports
      auth: {
          user: 'noreply@arabaparcasibul.com',
          pass: 'NoReply153624'
      },
      tls: {
        rejectUnauthorized: false
      }
  });

  // setup email data with unicode symbols
  let mailOptions = {
      from: '"Arabaparçasıbul.com" <noreply@arabaparcasibul.com>', // sender address
      to: req.body.email, 
      bcc: 'burakkp@ymail.com',
      subject: 'Arabaparçası Talebi', // Subject line
      html: output // html body
  };

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
          return console.log(error);
      }
      console.log('Message sent: %s', info.messageId);
      console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

      res.render('home', {msg: 'Talebiniz Tarafımızdan İlgili Parçacılara iletildi. En geç 1 gün içerisinde sizlere dönüş yapılacaktır.'});
  });

})