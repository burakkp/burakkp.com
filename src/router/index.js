import Vue from 'vue'
import Router from 'vue-router'
import VueHead from 'vue-head'
import VueFractionGrid from 'vue-fraction-grid'
import Vuebar from 'vuebar'
import VueClazyLoad from 'vue-clazy-load'
import Home from '@/components/Home'
import About from '@/components/About'
import Contact from '@/components/Contact'
import Services from '@/components/Services'
import Hireme from '@/components/Hireme'
import Header from '@/components/Header'
import Footer from '@/components/Footer'

Vue.use(VueHead)
Vue.use(Router)
Vue.use(Vuebar)
Vue.use(VueClazyLoad)
Vue.use(VueFractionGrid, {
  container: '1170px',
  gutter: '0px',
  approach: 'mobile-first',
  breakpoints: {
    compact: '0px 421px',
    tablet: '767px 1024px',
    desktop: '1025px'
  }
})

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      components: {
        default: Home,
        top: Header,
        bottom: Footer
      }
    },
    {
      path: '/About',
      name: 'About',
      components: {
        default: About,
        top: Header,
        bottom: Footer
      }
    },
    {
      path: '/Contact',
      name: 'Contact',
      components: {
        default: Contact,
        top: Header,
        bottom: Footer
      }
    },
    {
      path: '/Services',
      name: 'Services',
      components: {
        default: Services,
        top: Header,
        bottom: Footer
      }
    },
    {
      path: '/Hireme',
      name: 'Hireme',
      components: {
        default: Hireme,
        top: Header,
        bottom: Footer
      }
    }
  ],
  mode: 'history'
})
